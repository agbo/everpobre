//
//  NotesViewController.swift
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 9/13/16.
//  Copyright © 2016 KeepCoding. All rights reserved.
//

import UIKit

class NotesViewController: CoreDataTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let fc = fetchedResultsController else {
            return
        }
        
        title = (fc.fetchedObjects?.first as? Note)?.notebook?.name
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Id
        let cellId = "NoteCell"
        
        // La nota
        let note = fetchedResultsController?.object(at: indexPath) as! Note
        
        // La celda
        var cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        if cell == nil{
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellId)
        }
        
        // Los sincronizamos
        cell?.imageView?.image = note.photo?.image
        cell?.textLabel?.text = note.text
        
        // La devolvemos
        return cell!
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Averiguar la nota
        let note = fetchedResultsController?.object(at: indexPath) as! Note
        
        
        // Crear el VC
        let vc = NoteViewController(model: note)
        
        // Mostrarlo
        navigationController?.pushViewController(vc, animated: true)
        
    }
}




















