//
//  Notebook+CoreDataClass.swift
//  Everpobre
//
//  Created by Fernando Rodríguez Romero on 9/6/16.
//  Copyright © 2016 KeepCoding. All rights reserved.
//

import Foundation
import CoreData

@objc
public class Notebook: NSManagedObject {

    static let entityName = "Notebook"
    
    convenience init(name: String, inContext context: NSManagedObjectContext){
        
        // Necesitamos la entidad de Notebook
        let entity = NSEntityDescription.entity(forEntityName: Notebook.entityName, in: context)!
        
        // llamamos a super
        self.init(entity: entity, insertInto: context)
        
        
        
        // Asignamos valores a las fechas y el nombre
        creationDate = NSDate()
        modificationDate = NSDate()
        self.name = name
        
    }
}


//MARK: - KVO
extension Notebook{
    static func observableKeys() -> [String] {return ["name", "notes"]}
    
    func setupKVO(){
        
        // alta en las notificaciones 
        // para algunas propiedades
        // Deberes: Usar un la función map
        for key in Notebook.observableKeys(){
            self.addObserver(self, forKeyPath: key,
                             options: [], context: nil)
        }
        
        
    }
    
    func teardownKVO(){
    
        // Baja en todas las notificaciones
        for key in Notebook.observableKeys(){
            self.removeObserver(self, forKeyPath: key)
        }
        
    
    }
    
    public override func observeValue(forKeyPath keyPath: String?,
                                      of object: Any?,
                                      change: [NSKeyValueChangeKey : Any]?,
                                      context: UnsafeMutableRawPointer?) {
        
        // actualizar modificationDate
        modificationDate = NSDate()
        
    }
    
}


//MARK: - Lifecycle
extension Notebook{
    
    // Se llama una sola vez
    public override func awakeFromInsert() {
        super.awakeFromInsert()
        
        setupKVO()
    }
    
    // Se llama un huevo de veces
    public override func awakeFromFetch() {
        super.awakeFromFetch()
        
        setupKVO()
    }
    
    public override func willTurnIntoFault() {
        super.willTurnIntoFault()
        
        teardownKVO()
    }
}
















